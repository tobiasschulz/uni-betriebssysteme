#define _GNU_SOURCE

#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <sys/stat.h>
#include <string.h>
#include <strings.h>
#include "../common/def.h"

#define PERM_MASK (S_ISUID | S_ISGID | S_IRWXU | S_IRWXG | S_IRWXO)
#define LIST_RECURSION_LEVEL 255

int compare_strings(const void *first, const void *second) {
  return strcasecmp(*(char**)first, *(char**)second);
}

int list_recursive(char *basepath, size_t depth) {
  if (depth > LIST_RECURSION_LEVEL) return EXIT_FAILURE;

  size_t size = 0;
  char **entries = malloc(1 * sizeof(*entries));
  if (!entries) PERROR_RETURN("malloc failed");

  // read dir entries
  DIR *dir = opendir(basepath);
  if (!dir) PERROR_RETURN("opendir failed");
  struct dirent *entry;
  char *path;
  while ((entry = readdir(dir))) {
    if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
      continue;

    entries = realloc(entries, (++size) * sizeof(*entries));
    if (!entries) PERROR_RETURN("realloc failed");

    path = malloc(strlen(basepath) + strlen(entry->d_name) + 2);
    if (!path) PERROR_RETURN("malloc failed");
    strcpy(path, basepath);
    strcat(path, "/");
    strcat(path, entry->d_name);
    entries[size - 1] = path;
  }
  if (errno) PERROR_RETURN("readdir failed");
  closedir(dir);

  // sort dir entries
  qsort(entries, size, sizeof *entries, &compare_strings);

  // output dir entries
  struct stat statbuf;
  char type;
  for (size_t i = 0; i < size; ++i) {
    path = entries[i];
    if (lstat(path, &statbuf)) PERROR_RETURN("lstat failed");

    if      (S_ISDIR(statbuf.st_mode)) type = 'd';
    else if (S_ISREG(statbuf.st_mode)) type = 'f';
    else                               type = '?';

    printf("%c %04o %s\n", type, statbuf.st_mode & PERM_MASK, path);
    if (type == 'd')
      list_recursive(path, depth + 1);
    free(entries[i]);
  }
  free(entries);
  return EXIT_SUCCESS;
}

int main()
{
  return list_recursive(".", 0);
}
