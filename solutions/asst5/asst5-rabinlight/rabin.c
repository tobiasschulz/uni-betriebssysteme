#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "../common/def.h"
#include "rabin.h"

int main(int argc, char **argv)
{
  if (argc < 2) {
    fprintf(stderr, "Usage: %s input_file\n", argv[0]);
    return EXIT_FAILURE;
  }

  // open
  int fd = open(argv[1], O_RDONLY, (mode_t)0400);
  if (fd < 0) PERROR_EXIT("open");

  // get file size
  off_t len = lseek(fd, 0, SEEK_END);
  if (len == (off_t)-1) PERROR_EXIT("lseek");

  // map to memory
  unsigned char* data = mmap(0, len, PROT_READ, MAP_SHARED, fd, 0);
  if (data == MAP_FAILED) PERROR_EXIT("mmap");

  //====== calculate splitting points ======//

#define N 48

  unsigned int chunk_size = 0;
  unsigned int checksum = 0;
  for (size_t i = 0; i < (size_t)len; ++i) {
    chunk_size++;
    checksum += data[i];
    if (chunk_size < N)
      continue;

    if (chunk_size > N)
      checksum -= data[i - N];

    if (checksum & 0x1000)
      continue;

    printf("%u\n", (unsigned int)i);
    // "skip 48 bytes". This is not really precise,
    // I interpret it as "don't touch the following 48
    // bytes at all and reset the checksum"
    i += N + 1;    // leave a gap of 48 bytes
    checksum = 0;
    chunk_size = 0;
  }

  // file will get closed and unmapped by OS. It would be pretentious to do
  // this explicitely here, because we didn't do it at all the other exit
  // points. If we wanted, we could use
  //   munmap(data, len); close(fd);
  return 0;
}
