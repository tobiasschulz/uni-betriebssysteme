#define _POSIX_C_SOURCE 199309L
#include <fcntl.h>
#include <unistd.h>
#include "../common/def.h"
#include "splitter.h"

#define BUF_SIZE 0x1000

static int writeall(int fd, void *buffer, size_t size) {
  while (size) {
    size_t written = write(fd, buffer, size);
    if (written <= 0)
      return -written + 1; // 1 = EOF, 2 = error
    size -= written;
    buffer += written;
  }
  return 0; // success
}

static int open_chunk_file(unsigned int chunk_number) {
  char buf[32];
  sprintf(buf, "%u", chunk_number);
  return open(buf, O_WRONLY | O_TRUNC | O_CREAT, (mode_t)0664);
}

int main(int argc, char **argv)
{
  if (argc < 2) {
    fprintf(stderr, "Usage: %s input_file < splitpoints.txt\n", argv[0]);
    return EXIT_FAILURE;
  }

  // open input file
  int infile = open(argv[1], O_RDONLY, (mode_t)0400);
  if (infile < 0) PERROR_EXIT("Error opening input file");

  // get file size
  off_t len = lseek(infile, 0, SEEK_END);
  if (len == (off_t)-1) PERROR_EXIT("lseek error");

  // rewind
  if (lseek(infile, 0, SEEK_SET)) PERROR_EXIT("Error while rewinding");

  // read splitting points from STDIN and split the file accordingly
  size_t split_from = 0;
  unsigned int chunk_number = 1;
  void *buffer = malloc(BUF_SIZE);
  if (!buffer) PERROR_EXIT("malloc error");

  while (split_from < (size_t)len) {
    unsigned int split_point;

    // read split point
    int ret = scanf("%u", &split_point);
    if (ret == EOF && ferror(stdin))
      PERROR_EXIT("Error reading from STDIN")
    else if (ret == EOF)
      // no more split points, but we need to save the chunk
      // after the last one as well
      split_point = len - 1;
    else if (ret != 1) {
      fprintf(stderr, "Invalid input format\n");
      return EXIT_FAILURE;
    }

    size_t chunk_size = split_point - split_from + 1;
    // open output file
    int outfile = open_chunk_file(chunk_number);
    if (outfile < 0) PERROR_EXIT("Opening chunk file for writing");

    // copy loop: from in to out
    size_t yet_to_read = chunk_size;
    int read_bytes;
    while (yet_to_read) {
      read_bytes = read(infile, buffer, MIN(yet_to_read, 0x1000));
      if (read_bytes < 0) PERROR_EXIT("read");
      if (read_bytes == 0) {
        fprintf(stderr, "Unexpected end of input file.\n");
        return EXIT_FAILURE;
      }
      yet_to_read -= read_bytes;

      if (writeall(outfile, buffer, read_bytes)) {
        fprintf(stderr, "Writing error");
        return EXIT_FAILURE;
      }
    }

    ++chunk_number;
    split_from += chunk_size;
  }

  // file will get closed by OS. It would be pretentious to do this explicitely
  // here, because we didn't do it at all the other exit points.
  return 0;
}
