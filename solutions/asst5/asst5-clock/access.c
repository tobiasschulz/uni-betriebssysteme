#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include "access.h"

void reportMiss ( unsigned faultedPage );
void reportHit ( unsigned page, unsigned frame );
void reportEviction ( unsigned page, unsigned frame );

#define INVALID_PAGE_ID PAGES

// we use the clock as an inverted page table for simplicity
static struct
{
  uint16_t page;
  uint8_t bit;
} clock[FRAMES];

static size_t hand;

void init ()
{
  // init clock to all frames unallocated
  for (size_t i = 0; i < FRAMES; ++i)
    clock[i].page = (uint16_t)INVALID_PAGE_ID;
}


void clean () { }

// helper
static void advance() {
  hand = (hand + 1) % FRAMES;
}

void access( unsigned pn )
{
  // inverted table lookup
  for (size_t i = 0; i < FRAMES; ++i) {
    if (clock[i].page == pn) {
      // mark and return
      clock[i].bit = 1;
      reportHit(pn, i);
      return;
    }
  }

  reportMiss(pn);

  // not all frames are allocated yet
  if (clock[hand].page == INVALID_PAGE_ID) {
    clock[hand].page = pn;
    clock[hand].bit = 1;
    advance();
    return;
  }

  // clock algorithm
  while (clock[hand].bit) {
    clock[hand].bit = 0;
    advance();
  }

  reportEviction(clock[hand].page, hand);
  clock[hand].page = pn;
  clock[hand].bit = 1;
  advance();
}


void reportMiss ( unsigned faultedPage )
{
  printf("miss %u\n", faultedPage);
}

void reportHit ( unsigned page, unsigned frame )
{
  printf("hit %u %u\n", page, frame);
}

void reportEviction ( unsigned page, unsigned frame )
{
  printf("evicted %u %u\n", page, frame);
}
