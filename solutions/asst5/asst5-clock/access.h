#ifndef ACCESS_H
#define ACCESS_H

#define PAGES 256
#define FRAMES 16

void init ();
void clean ();
void access( unsigned pn );

#endif
