#include <stdio.h>
#include <inttypes.h>
#include "access.h"

int main()
{
  unsigned n;

  init();

  while( scanf("%u", &n) == 1 )
    access( n );

  clean();

  return 0;
}
