#include "segments.h"
#include <string.h>
#include "../common/macros.h"

extern struct segment segment_table[NSEGMENTS];

int main()
{
  // fill segment table
	memset(segment_table, 0, sizeof(segment_table));

  segment_table[0].base  = 0x0000;
  segment_table[0].limit = 0x0120;

  segment_table[1].base  = 0x0140;
  segment_table[1].limit = 0x0860;

  segment_table[4].base  = 0x2050;
  segment_table[4].limit = 0x1950;

  segment_table[7].base  = 0x6000;
  segment_table[7].limit = 0x0666;

  // tests
  ASSERT(segment_number(0xf999) == 7);
  ASSERT(offset(0xf999) == 0x1999);
  ASSERT(!isValid(0xf999));
  ASSERT(physical_addr(0xf999) == 0);

  ASSERT(segment_number(0x2020) == 1);
  ASSERT(offset(0x2020) == 0x20);
  ASSERT(isValid(0x2020));
  ASSERT(physical_addr(0x2020) == 0x160);

  ASSERT(segment_number(0x2859) == 1);
  ASSERT(offset(0x2859) == 0x859);
  ASSERT(isValid(0x2859));
  ASSERT(physical_addr(0x2859) == 0x0999);

  ASSERT(segment_number(0x4000) == 2);
  ASSERT(offset(0x4000) == 0x0);
  ASSERT(!isValid(0x4000));
  ASSERT(physical_addr(0x4000) == 0);

	return 0;
}
