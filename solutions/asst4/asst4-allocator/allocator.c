#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>

#include "allocator.h"

#define MEM_BLOCK_SIZE 1024

typedef struct _mem_block_s {
  char    proc_id;
  void    *base_addr;
  size_t  size;
} mem_block_s;

typedef struct {
  mem_block_s *blocks[MEM_SIZE];
} alloc_ctx_s;

// enable = {0} syntax (http://ex-parrot.com/~chris/random/initialise.html)
#pragma GCC diagnostic ignored "-Wmissing-braces"

alloc_ctx_s alloc_ctx = {0}; // valid C99 here, initializes
                             // everything to zeroes

int init_allocator()
{
  return 0; // done.
}

void cleanup_allocator()
{
  for (int i = 0; i < MEM_SIZE; ++i)
    release_partition(alloc_ctx.blocks[i]);
}

/**
 * Create and store a new memory information block
 * in our memory allocation table
 */
void * store_new_block(size_t offset,
                       size_t size,
                       char id)
{
  mem_block_s *block = malloc(sizeof *block);

  if (!block)
    return NULL; // :(

  block->proc_id   = id;
  block->base_addr = (void*)(offset * MEM_BLOCK_SIZE);
  block->size      = size;

  for (size_t i = offset; i < offset + size; ++i) {
    // mark blocks as reserved
    alloc_ctx.blocks[i] = block;
  }

  return block->base_addr;
}

/**
 * Mechanism to find the first location for a block of the
 * given size.
 */
int find_first_fit(size_t size,
                   size_t *offset)
{
  size_t space = 0;

  for (size_t i = 1; i < MEM_SIZE; ++i) {
    if (alloc_ctx.blocks[i]) {
      // current block is occupied
      space = 0;
      continue;
    }

    // enough?
    if (++space >= size) {
      *offset = i + 1 - space;
      return 1;
    }
  }

  return 0; // fallback
}

/**
 * Mechanism to find the location with best or worst fit for
 * a block of the given size.
 *   prefer_best == true  => best fit
 *   prefer_best == false => worst fit
 */
int find_best_or_worst_fit(size_t size,
                           size_t *offset,
                           bool prefer_best)
{
  int best_offset = -1;

  // initialize optimum to maximum or minimum possible value
  size_t best_space  = prefer_best ? (size_t)(-1) : 0;
  size_t space = 0;

  for (size_t i = 1; i <= MEM_SIZE; ++i) {
    if (i < MEM_SIZE && !alloc_ctx.blocks[i]) {
      ++space;
      continue;
    }

    // when we get here, current block is occupied or
    // we finished iteration, so we need to check
    // whether the last gap was a good/bad fit

    // was the last gap a better fit than the saved one?
    if (space >= size
          && (space < best_space ? prefer_best : !prefer_best)) {
      // yes, save!
      best_offset = i - space;
      best_space  = space;
    }

    space = 0;
  }

  if (best_offset < 0)
    return 0; // failure

  *offset = best_offset;
  return 1;
}

// policies only differ in the algorithm
// they use to find a free place for the new memory
// block.
#if POLICY == FIRST_FIT
#  define FIND_LOCATION find_first_fit
#elif POLICY == BEST_FIT
#  define FIND_LOCATION(size, offset)                 \
             find_best_or_worst_fit((size), (offset),  \
                                    true)
#else
#  define FIND_LOCATION(size, offset)                 \
             find_best_or_worst_fit((size), (offset),  \
                                    false)
#endif

/**
 * Allocate a block of memory. NULL is returned on
 * error, so NULL is not a valid memory location!!11einself
 * This means that the first allocable memory location
 * is MEM_BLOCK_SIZE.
 */
void * allocate_partition(unsigned int size, char id)
{
  size_t offset;

  // input check
  if (size == 0)
    return NULL; // error!

  if (!FIND_LOCATION(size, &offset))
    // no place found :/
    return NULL; // error!

  return store_new_block(offset, size, id);
}

void release_partition(void * addr)
{
  size_t offset = (size_t)addr / MEM_BLOCK_SIZE;
  size_t block_size;

  // input checks
  if (!addr)
    // don't free null ptr, as it is no valid location!
    return;
  if (offset >= MEM_SIZE)
    return;
  if (!alloc_ctx.blocks[offset])
    return;

  block_size = alloc_ctx.blocks[offset]->size;
  for (size_t j = offset; j < offset + block_size; ++j)
    alloc_ctx.blocks[j] = NULL;

  free(alloc_ctx.blocks[offset]);
}

void print_memory_map()
{
  for (int i = 1; i < MEM_SIZE; ++i)
    printf("%c", alloc_ctx.blocks[i]
                   ? alloc_ctx.blocks[i]->proc_id
                   : '0');
  printf("\n");
}
