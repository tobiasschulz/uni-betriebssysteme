#ifndef _MACROS_H
#define _MACROS_H

#include <stdio.h>

#define ANSI_RED   "\033[31m"
#define ANSI_GREEN "\033[32m"
#define ANSI_CLR   "\033[0m"

#define NUM_OF(x) (sizeof(x)/sizeof(*(x)))
#define ASSERT(cond)                                        \
  if (cond) {                                               \
    printf(ANSI_GREEN "Passed: %s" ANSI_CLR "\n", #cond);   \
  } else {                                                  \
    printf(ANSI_RED "Failed: %s" ANSI_CLR "\n", #cond);     \
  }

#endif
