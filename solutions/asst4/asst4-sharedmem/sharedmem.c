#define _GNU_SOURCE

#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

void server();
void client();


#define SHAREDMEM_SIZE 4096

// read/write access for the owner
#define SHM_PERM (S_IRUSR | S_IWUSR)

// global context will be located
// in a single shared segment
struct {
  int a, b;
  sem_t sem_client;
  sem_t sem_server;
} *ctx;

int main()
{
  int shm_id = 0;

	/* Don't remove this! We need it for testing. */
	setbuf(stdout, NULL);

  // set up a shared memory segment
  shm_id = shmget(IPC_PRIVATE,    // initialize a new, anonymous
                                  // shared memory segment
                  SHAREDMEM_SIZE, // size of the new segment
                  SHM_PERM        // set permissions according
                                  // to SHM_PERM
                  );

  // map shared memory
  ctx = shmat(shm_id,  // the ID returned by shmget
              NULL,    // we don't care about the mapped
                       // location
              0        // attach with read/write access
              );

  // set up two binary semaphores for
  // communication between the processes.
  sem_init(&ctx->sem_client,
           1,  // shared between processes
           0); // initial value 0 (locked)
  sem_init(&ctx->sem_server,
           1,  // shared between processes
           0); // initial value 0 (locked)

  pid_t pid = fork();
  switch (pid) {
  case 0:
		client();
    goto cleanup_both;
  case -1:
    perror("fork");
    goto cleanup_server;
  default:
    server();
    waitpid( pid, NULL, 0 );
    printf("Bye.\n");
    goto cleanup_server;
  }

  // free shared resources
cleanup_server:
  sem_destroy(&ctx->sem_client);
  sem_destroy(&ctx->sem_server);
cleanup_both:
  shmdt(ctx);

	return 0;
}

// helper macro
#define DO_EXIT() (ctx->a == -1 && ctx->b == -1)

void server()
{
  for(;;) {
    sem_wait(&ctx->sem_server); // wait for client

    if (DO_EXIT()) // exit?
      return;
    printf("sum = %i\n", ctx->a + ctx->b);

    sem_post(&ctx->sem_client); // trigger client
  }
}

void client()
{
  for(;;) {
    printf("Please enter two integers seperated by whitespace "
           "(enter -1 -1 to exit):\n");

    // read user input
    if (2 != scanf("%i %i", &ctx->a, &ctx->b)) {
      // on error, discard the rest of the line
      // and try again
      printf("Input error! Try again.\n");
      while (getchar() != '\n');

      // start over
      continue;
    }

    // on success, call the server!
    sem_post(&ctx->sem_server); // trigger server

    // exit after calling the server (so it has a chance
    // to quit) but _before_ waiting for the server again
    // (as obviously, the server has exited by now)
    if (DO_EXIT())
      break;

    sem_wait(&ctx->sem_client); // wait for server
  }

	exit(EXIT_SUCCESS);
}

// clean up preprocessor
#undef KEEP_RUNNING
