#include "pointers.h"
#include "../common/macros.h"

int main()
{
  int array1[] = { 5, 6, -6, -12, 100, 10, -1000, 2, 9 };
  int array2[] = { 2, 3, 4, 5, 6, 7, 7 };

  unsigned int size1 = NUM_OF(array1);
  unsigned int size2 = NUM_OF(array2);
  int *p_array1[size1];
  int *p_array2[size2];

  for (unsigned int i = 0; i < size1; ++i)
    p_array1[i] = &array1[i];
  for (unsigned int i = 0; i < size2; ++i)
    p_array2[i] = &array2[i];

  ASSERT(average(array1, size1) == -98,     "average [1]");
  ASSERT(average(array2, size2) == 4,       "average [2]");
  ASSERT(p_average(p_array1, size1) == -98, "p_average [1]");
  ASSERT(p_average(p_array2, size2) == 4,   "p_average [2]");

	return 0;
}
