#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "bits.h"
#include "../common/macros.h"

uint64_t rot_wrapper(uint64_t value, unsigned int n)
{
  rot(&value, n);
  return value;
}

int main()
{
  ASSERT(roundDown(100) == 0,     "roundDown [1]");
  ASSERT(roundDown(4096) == 4096, "roundDown [2]");
  ASSERT(roundDown(4097) == 4096, "roundDown [3]");
  ASSERT(roundDown(8191) == 4096, "roundDown [4]");
  ASSERT(roundDown(8192) == 8192, "roundDown [5]");
  ASSERT(roundDown(8193) == 8192, "roundDown [6]");

  {
    uint8_t array1[] = { 0b10010111, 0b01110110, 0b00001000, 0b00110011 };
    uint32_t check1 = *(uint32_t*)array1;

    ASSERT(getN(array1, 0) == 1, "getN [1]");
    ASSERT(getN(array1, 1) == 0, "getN [2]");
    ASSERT(getN(array1, 2) == 0, "getN [3]");
    ASSERT(getN(array1, 3) == 1, "getN [4]");
    ASSERT(getN(array1, 8) == 0, "getN [5]");
    ASSERT(getN(array1, 9) == 1, "getN [6]");
    ASSERT(getN(array1, 11) == 1, "getN [7]");
    ASSERT(getN(array1, 19) == 0, "getN [8]");
    ASSERT(getN(array1, 20) == 1, "getN [9]");
    ASSERT(getN(array1, 29) == 0, "getN [10]");
    ASSERT(getN(array1, 30) == 1, "getN [11]");

    ASSERT(*(uint32_t*)array1 == check1, "consistency check [1]");

    setN(array1, 8);
    setN(array1, 19);
    setN(array1, 20);
    ASSERT(getN(array1, 8) == 1, "setN [1]");
    ASSERT(getN(array1, 19) == 1, "setN [2]");
    ASSERT(getN(array1, 20) == 1, "setN [3]");

    clrN(array1, 8);
    clrN(array1, 19);
    clrN(array1, 29);
    ASSERT(getN(array1, 8) == 0, "clrN [1]");
    ASSERT(getN(array1, 19) == 0, "clrN [2]");
    ASSERT(getN(array1, 29) == 0, "clrN [3]");

    ASSERT(*(uint32_t*)array1 == check1, "consistency check [2]");

    invert(array1, 5, 18);
    ASSERT(array1[0] == 0b10010000, "invert [1.1]");
    ASSERT(array1[1] == 0b10001001, "invert [1.2]");
    ASSERT(array1[2] == 0b11001000, "invert [1.3]");
    ASSERT(array1[3] == 0b00110011, "invert [1.4]");

    invert(array1, 5, 18);
    ASSERT(*(uint32_t*)array1 == check1, "consistency check [3]");
  }

  {
    unsigned int big_array_size = 2000000;
    uint8_t *big_array = malloc(big_array_size);
    bool_t success;

    if (!big_array) return 1;

    // check 1
    memset(big_array, 0xff, big_array_size);
    invert(big_array, 0, big_array_size*8);

    success = TRUE;
    for (unsigned int i = 0; i < big_array_size; ++i)
      if (big_array[i]) { success = FALSE; break; }
    ASSERT(success, "big array invert [1]");

    // check 2
    memset(big_array, 0, big_array_size);
    invert(big_array, 0, big_array_size*4);

    success = TRUE;
    for (unsigned int i = 0; i < big_array_size / 2; ++i)
      if (!big_array[i]) { success = FALSE; break; }
    for (unsigned int i = big_array_size / 2; i < big_array_size; ++i)
      if (big_array[i]) { success = FALSE; break; }
    ASSERT(success, "big array invert [2]");

    // check 3
    memset(big_array, 0x0f, big_array_size);
    invert(big_array, 0, big_array_size*8);

    success = TRUE;
    for (unsigned int i = 0; i < big_array_size / 2; ++i)
      if (big_array[i] != 0xf0) { success = FALSE; break; }
    ASSERT(success, "big array invert [3]");

    free(big_array);
  }

  ASSERT(rot_wrapper(0xf0000000f0000000, 8)    == 0x00f0000000f00000, "rot [1]");
  ASSERT(rot_wrapper(0xf0000000f000000f, 8)    == 0x0ff0000000f00000, "rot [2]");
  ASSERT(rot_wrapper(0xf0000000f000000f, -8)   == 0x000000f000000ff0, "rot [3]");
  ASSERT(rot_wrapper(0xf0000000f000000f, 64)   == 0xf0000000f000000f, "rot [4]");
  ASSERT(rot_wrapper(0xf0000000f000000f, -64)  == 0xf0000000f000000f, "rot [5]");
  ASSERT(rot_wrapper(0xf0000000f000000f, 128)  == 0xf0000000f000000f, "rot [6]");
  ASSERT(rot_wrapper(0xf0000000f000000f, -128) == 0xf0000000f000000f, "rot [7]");

	return 0;
}
