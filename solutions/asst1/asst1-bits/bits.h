#ifndef BITS_H
#define BITS_H

#include <stdint.h>

unsigned int roundDown ( unsigned int addr );

int getN ( uint8_t *A, unsigned int n );
void setN ( uint8_t *A, unsigned int n );
void clrN ( uint8_t *A, unsigned int n );

void invert ( uint8_t *A, unsigned int n, unsigned int p );

void rot ( uint64_t *i, int n );

#endif
