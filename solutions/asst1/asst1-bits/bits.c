#include <limits.h>
#include "bits.h"
#include <stdio.h>

unsigned int roundDown ( unsigned int addr )
{
  // create bitmask 0xfffff000 by using the two's complement
  // of 4096 (0x1000)
	return addr & -(unsigned int)0x1000;
}

void create_bit_mask(unsigned int bit_offset,    // in
                     unsigned int *byte_offset,  // out
                     uint8_t      *bit_mask)     // out
{
  // figure out which byte to manipulate
  if (byte_offset)
    *byte_offset = (bit_offset / CHAR_BIT);

  // calculate bit offset relative to the byte
  bit_offset = bit_offset % CHAR_BIT;

  // build up the bit mask for our byte
  if (bit_mask)
    *bit_mask = (1 << (CHAR_BIT - bit_offset - 1));
}

int getN ( uint8_t *A, unsigned int n )
{
  unsigned int byte_offset;
  uint8_t bit_mask;
  create_bit_mask(n, &byte_offset, &bit_mask);

  // apply bit mask and return boolean 1 or 0
  return (A[byte_offset] & bit_mask) ? 1 : 0;
}

void setN ( uint8_t *A, unsigned int n )
{
  unsigned int byte_offset;
  uint8_t bit_mask;
  create_bit_mask(n, &byte_offset, &bit_mask);

  // apply bit mask to set the specified bit
  A[byte_offset] |= bit_mask;
}

void clrN ( uint8_t *A, unsigned int n )
{
  unsigned int byte_offset;
  uint8_t bit_mask;
  create_bit_mask(n, &byte_offset, &bit_mask);

  // apply bit mask to clear the specified bit
  A[byte_offset] &= ~bit_mask;
}


void invert ( uint8_t *A, unsigned int n, unsigned int p )
{
  unsigned int byte_offset;
  uint8_t bit_mask;

  // could be made more efficient by masking whole bytes
  // at once
  for (unsigned int i = n; i < p; ++i) {
    create_bit_mask(i, &byte_offset, &bit_mask);
    A[byte_offset] ^= bit_mask;
  }
}


void rot ( uint64_t *i, int n )
{
  // 1. shift to right, lose some bits on the right margin
  // 2. shift to left by (64-n) bits, to get the bits that
  //    where pushed over the right margin
  // 3. OR those two masks together to get the final result
  *i = (*i >> n) | (*i << (64-n));
}
