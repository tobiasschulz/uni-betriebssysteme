#ifndef _MACROS_H
#define _MACROS_H

#include <stdio.h>

typedef char bool_t;
#define TRUE 1
#define FALSE 0

#define ANSI_RED "\033[31m"
#define ANSI_GREEN "\033[32m"
#define ANSI_CLR "\033[0m"

#define NUM_OF(x) (sizeof(x)/sizeof(*(x)))
#define ASSERT(cond, msg)                                               \
  if (cond) {                                                           \
    printf(ANSI_GREEN "Passed: %s (%s)" ANSI_CLR "\n", msg, #cond);     \
  } else {                                                              \
    printf(ANSI_RED "Failed: %s (%s)" ANSI_CLR "\n", msg, #cond);       \
  }

#endif
