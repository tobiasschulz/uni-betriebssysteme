#include <stdlib.h>
#include <stdio.h>
#include "datastruct.h"

#define FAIL_GRACELESSLY(msg) { perror(msg); exit(1); }

// define a cyclic singly linked list
// to make operations easier
process_s first = { -1, -1, &first };

/* insert process in data structure */
void insert ( int id, int priority )
{
  process_s *item = malloc(sizeof(process_s));
  process_s *prev = &first;

  if (!item)
    FAIL_GRACELESSLY("malloc failed");

  item->id = id;
  item->priority = priority;

  while (prev->next->priority >= priority)
    prev = prev->next;

  item->next = prev->next;
  prev->next = item;
}


/* return element with highest priority */
int front ()
{
  return first.next->id;
}


/* remove element with highest priority */
void pop ()
{
  process_s *item = first.next;
  if (item->id < 0) return;
  first.next = item->next;
  free(item);
}


/* remove element with given id */
void popId ( int id )
{
  process_s *prev = &first;
  process_s *item;

  while (prev->next->id != id)
    prev = prev->next;

  item = prev->next;
  prev->next = item->next;
  free(item);
}
