#ifndef DATASTRUCT_H
#define DATASTRUCT_H

typedef struct process_s_ {
  int id;
  int priority;
  struct process_s_ *next;
} process_s;

/* insert process in data structure */
void insert ( int id, int priority );

/* return element with highest priority */
int front ();

/* remove element with highest priority */
void pop ();

/* remove element with given id */
void popId ( int id );

#endif
