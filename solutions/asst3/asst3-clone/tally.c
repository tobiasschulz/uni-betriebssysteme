#define _GNU_SOURCE
#include <inttypes.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>

#include "tally.h"

sem_t sem; // semaphore must be global, because we can't
           // change signature of the increment function.
           // Why not keep all context in global namespace
           // then? Design failure?

void tally_init ()
{
  // set up semaphore
  if (sem_init(&sem, 0, 1)) {
    perror("sem_init failed");
    exit(1);
  }
}


/* modify this function to achieve correct parallel incrementation */
void increment ( uint64_t *tally, uint64_t steps )
{
	for( uint64_t i = 0; i < steps; i++ )
  {
    sem_wait(&sem); // enter CS
		*tally += 1;
    sem_post(&sem); // leave CS
	}
}


void tally_clean ()
{
  sem_destroy(&sem);
}
