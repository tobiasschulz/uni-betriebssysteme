#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>

#include "ult.h"

#define handle_error(msg) \
    do { perror(msg); exit(EXIT_FAILURE); } while (0)

int
main ()
{
	if ( createThreadPool( 9 ) == -1 )
	{
		handle_error("createThreadPool failed");
	}

	if ( runThreads() == -1 )
	{
		handle_error("interactive failed");
	}

	exit( EXIT_SUCCESS );
}

