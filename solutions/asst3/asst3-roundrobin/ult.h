#ifndef ULT_H
#define ULT_H

#define _GNU_SOURCE
#include <ucontext.h>

#define MAX_THREADS 32 /* including the idle thread */

typedef enum state
{
	terminated, ready, running, waiting
} state_t;


struct Context
{
	ucontext_t context;
	state_t    state;
	int        blockTime;
};

struct Context *threads[MAX_THREADS];

int  createThreadPool ( size_t numberOfWorkers );
void destroyThreadPool ();
int  runThreads ();

/* for manual testing */
int  block ( int tid, int time );
void manageBlockedThreads ();

#endif /* ULT_H */
