#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "processes.h"

void forkbomb()
{
  struct rlimit limit;

  // some self-restriction to keep a managable
  // number of processes
  limit.rlim_max = limit.rlim_cur = 500;
  if (setrlimit(RLIMIT_NPROC, &limit))
    perror("setrlimit");

  // haha. exponential.
  for (; fork() >= 0; fork(), fork());
}

/*
void setup_sigchld_handler();

void sigchld_handler(int unused) {
  (void)unused; // prevent error triggered by -Werror
  printf("handling zombie :)\n");
  while(waitpid(-1, NULL, WNOHANG) > 0);
  setup_sigchld_handler();
}

void setup_sigchld_handler() {
  struct sigaction action;
  action.sa_handler = &sigchld_handler;
  action.sa_flags = SA_NOCLDSTOP;
  if (sigaction(SIGCHLD, &action, NULL) < 0) {
    fprintf(stderr, "sigaction failed.\n");
    exit(1);
  }
}
*/

void spawnChildren( unsigned int n )
{
  // the following seems to be the most portable way
  // to handle zombie children according to
  //  http://www.faqs.org/faqs/unix-faq/faq/part3/section-13.html
  // Sadly, this does not work with pure C99, so the Makefile
  // would need to be adapted for this to work (-std=gnu99).
  //setup_sigchld_handler();

  for (unsigned int i = 0; i < n; ++i) {
    pid_t pid = fork();
    if (!pid) {      // child branch
      printf("%d\n", getpid());
      exit(0);
    } else {         // parent branch
      // less robust method of handling zombies (see above)
      waitpid(-1, NULL, 0);
      printf("I am the parent process of: %d\n", pid);
    }
  }
}
