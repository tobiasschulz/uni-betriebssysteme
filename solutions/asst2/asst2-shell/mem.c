#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mem.h"

error_t my_alloc(void **buffer, size_t size)
{
  if (!buffer)
    return E_INVALID_ARG;
  if (!(*buffer = malloc(size)))
    return E_OUT_OF_MEMORY;
  return E_SUCCESS;
}

error_t my_calloc(void **buffer, size_t size)
{
  TRY(my_alloc(buffer, size));
  memset(*buffer, 0, size);
  return E_SUCCESS;
}

error_t my_realloc(void **buffer, size_t size)
{
  void *new;
  if (!buffer)
    return E_INVALID_ARG;

  if (!(new = realloc(*buffer, size)))
    return E_OUT_OF_MEMORY;
  *buffer = new;
  return E_SUCCESS;
}

error_t my_free(void **buffer)
{
  if (!buffer)
    return E_INVALID_ARG;
  if (*buffer) // no freeing null-pointers!
    free(*buffer);
  *buffer = 0; // zero out to prevent double-free
  return E_SUCCESS;
}

