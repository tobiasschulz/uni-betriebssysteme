#ifndef _SHELL_H
#define _SHELL_H

#include "errors.h"

// documentation see shell.c
error_t parse_argv(char *cmdline, char ***argv);
error_t free_argv(char ***argv);
error_t read_line(FILE *stream, char **line_buffer);
int main();

#endif
