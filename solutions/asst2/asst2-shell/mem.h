#ifndef _MEM_H
#define _MEM_H

#include <stddef.h> /* for size_t */
#include "errors.h"

/**
 * Allocates a memory buffer
 *
 * @param buffer Pointer to a pointer that receives the allocated
 *               buffer
 * @param size   The size in bytes to allocate
 * @return error status
 */
error_t my_alloc(void **buffer, size_t size);

/**
 * Allocates a memory buffer (zero-initialized)
 *
 * @param buffer Pointer to a pointer that receives the allocated
 *               buffer
 * @param size   The size in bytes to allocate
 * @return error status
 */
error_t my_calloc(void **buffer, size_t size);

/**
 * Re-allocates a memory buffer. Only changes the pointer if
 * no error occured!
 *
 * @param buffer Pointer to a pointer that received buffer
 *               allocated by my_alloc
 * @param size   The size in bytes to allocate
 * @return error status
 */
error_t my_realloc(void **buffer, size_t size);

/**
 * Frees a buffer allocated by my_alloc and zeroes out the pointer
 * to prevent double-frees
 *
 * @param buffer Pointer to the buffer allocated by my_alloc
 */
error_t my_free(void **buffer);

#endif
