#ifndef _ERRORS_H
#define _ERRORS_H

// error handling
typedef int error_t;
enum error_e {
  E_SUCCESS = 0,
  E_FAILURE,
  E_OUT_OF_MEMORY,
  E_INVALID_ARG,
  E_EOF,
  E_STDLIB_ERROR,
};

// initialize a local error status variable
#define INIT_STATUS_SUCCESS error_t status = E_SUCCESS;
// call a function that returns error_t and return the
// result if an error occurs
#define TRY(x) { error_t status; if ((status = (x))) return status; }
// call a function returning error_t, save the result in a local status
// variable and break if an error occured
#define TRY_BREAK(x) if ((status = (x))) break;

#endif
