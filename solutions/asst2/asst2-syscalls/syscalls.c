#include <stdint.h>
#include <stddef.h> /* for NULL */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include "syscalls.h"

typedef int BOOL;

void print3( char a, uint8_t b, int8_t c )
{
  printf("%c %d %d\n", a, b, c);
}


/**
 * Formats an integer to a string (no bounds checking!)
 * Returns the number of bytes written.
 */
size_t format_int(int x,
                  unsigned int base,
                  char *out,
                  size_t min_width)
{
  char *o = out;
  size_t written_size;
  int y;

  // handle negative numbers and zero
  if (x < 0) {
    *(o++) = '-';
    x = -x;
  } else if (x == 0) {
    if (!min_width) min_width = 1;
  }

  // find the place after the last digit
  y = x;
  while (y || min_width) {
    *(o++) = '0';
    y /= base;
    if (min_width) --min_width;
  }

  // save return value
  written_size = o - out;

  // write digits from back to front
  while (x) {
    *(--o) = '0' + x % base;
    x /= base;
  }

  return written_size;
}


// 2 spaces + newline + null-termination
// + 1 single char
// + 2 ints with 4 chars maximum each (including negative sign)
#define MAX_WRITE3_SIZE (sizeof("  \n") + 1 + 2*4 + 100)

void write3( char a, uint8_t b, int8_t c )
{
  // reserve space for a 2 spaces, newline, null termination,
  // a single char and two ints of 1 byte (4 chars maximum each)
  char buf[MAX_WRITE3_SIZE];
  char *s = buf;

  // build up buffer
  *(s++) = a;
  *(s++) = ' ';
  s += format_int(b, 10, s, 0);
  *(s++) = ' ';
  s += format_int(c, 10, s, 0);
  *(s++) = '\n';
  *(s++) = '\0';

  // write buffer
  write(STDOUT_FILENO, buf, s - buf);
}


// seperators + null-termination
// + 5*2 chars for DD, MM, hh, mm, ss
// + 4 chars for YYYY
#define MAX_DATE_SIZE (sizeof(".. - ::") + 5*2 + 4)

/**
 * Returns the current time formatted as a string.
 *
 * Memory is allocated via malloc and must be freed by the caller
 * using the free function provided by libc.
 */
char* getTime()
{
  // reserve space for the date.
  // This buffer is the caller's responsibility.
  char *buf = (char*)malloc(MAX_DATE_SIZE);
  char *s = buf;
  struct timeval now;
  time_t timestamp;
  struct tm *time_parts;

  gettimeofday(&now, NULL);
  timestamp = (time_t)now.tv_sec;
  time_parts = localtime(&timestamp);

  // build up the date string
  s += format_int(time_parts->tm_mday, 10, s, 2);
  *(s++) = '.';
  s += format_int(time_parts->tm_mon + 1, 10, s, 2);
  *(s++) = '.';
  s += format_int(time_parts->tm_year + 1900, 10, s, 0);
  *(s++) = ' ';
  *(s++) = '-';
  *(s++) = ' ';
  s += format_int(time_parts->tm_hour, 10, s, 2);
  *(s++) = ':';
  s += format_int(time_parts->tm_min, 10, s, 2);
  *(s++) = ':';
  s += format_int(time_parts->tm_sec, 10, s, 2);
  *(s++) = '\0';

  return buf;
}
