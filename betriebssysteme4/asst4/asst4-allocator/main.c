#include "allocator.h"


int main()
{
	void * foo;

	/*
	 * Put your test code here.
	 */
	

	/* example from assignment */
	init_allocator();
	allocate_partition( 3, 'A' );
	foo = allocate_partition( 1, 'D' );
	allocate_partition( 2, 'C' );
	allocate_partition( 1, 'B' );
	release_partition( foo );
	foo = allocate_partition( 1, 'E' );

	print_memory_map();

	release_partition( foo );
	foo = allocate_partition( 2, 'F' );

	print_memory_map();

	cleanup_allocator();
	print_memory_map();

	return 0;
}
