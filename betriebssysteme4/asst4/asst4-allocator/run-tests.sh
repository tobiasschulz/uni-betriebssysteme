#!/bin/bash

cd `dirname $0`

echo first fit:
./allocator-tutor-firstfit

echo best fit:
./allocator-tutor-bestfit

echo worst fit:
./allocator-tutor-worstfit

