#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>

#include "allocator.h"

typedef struct _partition {
	char id;

	void* base_addr;
	void* limit_addr;

	struct _partition* prev;
	struct _partition* next;
} partition;

partition* begin = 0;

int init_allocator()
{
	begin = malloc(sizeof(partition));
	begin->id = '0';
	begin->base_addr = 0;
	begin->limit_addr = (void*)MEM_SIZE;
	begin->prev = 0;
	begin->next = 0;
	return 0;
}

void cleanup_allocator()
{
	for (partition* i = begin; i != NULL;) {
		partition* next = i->next;
		free(i);
		i = next;
	}
	begin = 0;
}

void insert_null_partition_after(partition* i, unsigned int i_new_size) {
	void* limit = i->limit_addr;
	i->limit_addr = (void*)((uintptr_t)i->base_addr + i_new_size);

	if (i->next && i->next->id == '0') {
		i->next->base_addr = i->limit_addr + 1;
	}
	else {
		partition* j = malloc(sizeof(partition));
		j->id = '0';

		j->limit_addr = limit;
		j->base_addr = i->limit_addr;

		j->prev = i;
		j->next = i->next;
		if (j->next != NULL)
			j->next->prev = j;
		i->next = j;
	}
}

void* allocate_in_null_partition(partition* i, unsigned int size, char id) {
	i->id = id;
	if ((uintptr_t)(i->limit_addr - i->base_addr) > size) {
		insert_null_partition_after(i, size);
	}
	return i->base_addr;
}

#if POLICY == FIRST_FIT
void * allocate_partition(unsigned int size, char id)
{
	for (partition* i = begin; i != NULL; i = i->next) {
		if (i->id == '0' && (uintptr_t)(i->limit_addr - i->base_addr) >= size) {
			return allocate_in_null_partition(i, size, id);
		}
	}
	return 0;
}
#elif POLICY == BEST_FIT
void * allocate_partition(unsigned int size, char id)
{
	partition* best = 0;
	uintptr_t bestfit = MEM_SIZE;
	for (partition* i = begin; i != NULL; i = i->next) {
		uintptr_t partsize = i->limit_addr - i->base_addr;
		uintptr_t fit = partsize - size;
		if (i->id == '0' && partsize >= size) {
			if (fit < bestfit) {
				bestfit = fit;
				best = i;
			}
		}
	}
	if (best != NULL)
		return allocate_in_null_partition(best, size, id);
	else
		return 0;
}
#else
void * allocate_partition(unsigned int size, char id)
{
	partition* worst = 0;
	uintptr_t worstfit = 0;
	for (partition* i = begin; i != NULL; i = i->next) {
		uintptr_t partsize = i->limit_addr - i->base_addr;
		uintptr_t fit = partsize - size;
		if (i->id == '0' && partsize >= size) {
			if (fit > worstfit) {
				worstfit = fit;
				worst = i;
			}
		}
	}
	if (worst != NULL)
		return allocate_in_null_partition(worst, size, id);
	else
		return 0;
}
#endif

void merge_partitions(partition* curr, partition* next) {
	curr->limit_addr = next->limit_addr;
	curr->next = next->next;
	if (next->next != NULL)
		next->next->prev = curr;

	free(next);
	next = 0;
}

void release_partition(void * addr)
{
	for (partition* i = begin; i != NULL; i = i->next) {
		if (i->base_addr == addr && i->id != '0') {
			i->id = '0';
			partition* current = (i->prev != NULL && i->prev->id == '0') ? i->prev : i;
			while (current != NULL && current->id == '0' && current->next != NULL && current->next->id == '0') {
				merge_partitions(current, current->next);
			}
			break;
		}
	}
}

void print_memory_map()
{
	for (partition* i = begin; i != NULL; i = i->next) {
		for (uintptr_t j = (uintptr_t)i->base_addr; j < (uintptr_t)i->limit_addr; ++j) {
			printf("%c", i->id);
		}
	}
	printf("\n");
}
