#include "segments.h"
#include <stdio.h>
#include <string.h>

extern struct segment segment_table[NSEGMENTS];

#define ASSERT(cond)                                        \
  if (cond) {                                               \
      printf("Passed: %s\n", #cond);   \
        } else {                                                  \
            printf("Failed: %s\n", #cond);     \
              }


int main ()
{
  /*
   * Put your test code here.
   */

  /* dummy case: limits all zeros, thus all segments invalid */
  memset (segment_table, 0, sizeof (segment_table));


  segment_table[0].base = 0x0000;
  segment_table[0].limit = 0x0120;

  segment_table[1].base = 0x0140;
  segment_table[1].limit = 0x0860;

  segment_table[4].base = 0x2050;
  segment_table[4].limit = 0x1950;

  segment_table[7].base = 0x6000;
  segment_table[7].limit = 0x0666;

  // tests
  ASSERT (segment_number (0x4123) == 4);
  ASSERT (offset (0x4123) == 0x123);
  ASSERT (isValid (0x4123));
  ASSERT (physical_addr (0x4123) == 0x2050 + 0x123);

  return 0;
}
