#include "segments.h"

struct segment segment_table[NSEGMENTS];

int segment_number( uint16_t addr )
{
	return addr >> 12;
}

int offset( uint16_t addr )
{
	return addr & 0x0fff;
}

int isValid( uint16_t addr )
{
	uint16_t limit = segment_table[segment_number(addr)].limit;
	int _offset = offset(addr);
	return _offset < limit;
}

int physical_addr( uint16_t addr )
{
	if (isValid(addr)) {
		uint16_t base = segment_table[segment_number(addr)].base;
		int _offset = offset(addr);
		return _offset + base;
	}
	else
		return 0;
}

