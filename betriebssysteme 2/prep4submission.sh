#!/bin/bash

##
#  This script may help you to submit your solution
#  correctly and to please your tutor.
#
#  What the script excatly performs:
#   It transforms the directory structure as provided
#   in the official tarball into the fashion required
#   for submission labeled with your matriculation number.
##

set -e

HERE=$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null && pwd)
PROG="${0##*/}"
VERSION="0.2.2"

trap bug_found ERR

function usage() {
    echo "$PROG [matriculation-no [path-to-solution]]"
}

function bug_found() {
    echo >&2 "ERROR: You found a bug in the $PROG script, please report"
    exit 99
}

#var, pred, question, [read-opts, [default]]
function ask_valid() {
    var="$1"
    pred="$2"
    question="$3"
    read_opts="$4"
    default="$5"
    while true; do
        echo -n "$question"
        if [ ! -z "$default" ]; then
            echo -n "[$default] "
        fi
        read $read_opts $var
        if [ -z "${!var}" -a ! -z "$default" ]; then
            export $var="$default"
        fi
        if $pred "${!var}"; then
            break
        fi
        echo "invalid input, try again"
    done
}

function check_matr() {
    if [[ "$1" =~ ^1[0-9]{6}$ ]]; then
        return 0
    else
        return 1
    fi
}

function check_path() {
    if [[ $(basename "$1") =~ ^asst[1-9]$ ]]; then
        return 0
    else
        return 1
    fi
}

function find_other_typed_files() {
    export non_supported_files=`find . -type f -print | xargs file -i | grep -E --invert-match "charset=($1)"`

    if [ -z "$non_supported_files" ]; then
	return 0
    else
	return 1
    fi
}

if [ "$1" = "--help" -o "$1" = "-h" ]; then
    usage
    exit 0
fi

ask_matr=1
guess_path=0

case $# in
    0)
        ask_matr=1
        guess_path=1
        ;;
    1)
        ask_matr=0
        guess_path=1
        ;;
    2)
        ask_matr=0
        guess_path=0
        ;;
    *)
        echo >&2 "ERROR: Too many arguments"
        usage
        exit 1
esac

if [ $ask_matr -eq 0 ]; then
    matr="$1"
    if ! check_matr "$matr"; then
        echo >&2 "WARNING: '$matr' is no matriculation number, asking you..."
        ask_matr=1
    fi
fi

if [ $ask_matr -ne 0 ]; then
    ask_valid matr check_matr "Your matriculation number? "
fi

if [ $guess_path -eq 0 ]; then
    path="$2"
else
    path="$HERE"
fi

if ! ( cd "$path" &> /dev/null && check_path "$(pwd)" ); then
    echo >&2 "WARNING: '$path' probably wrong"
    ask_valid path check_path "Path to your solution's top-level directory? " \
        -e "$HERE"
fi

cd "$path" &> /dev/null || { echo >&2 "ERROR: cannot chdir to '$path'"; \
                             exit 2; }

compatible_char_sets="us-ascii|utf-8"
if ! find_other_typed_files "$compatible_char_sets"; then
    echo >&2 "ERROR: some files are not in compatible charsets: $compatible_char_sets"
    echo >&2 $non_supported_files
    rm -rf "$tmp"
    exit 4
fi

path=$(pwd)
basedir=$(basename "$path")

# build (temporary) structure and archive
tmp="$(mktemp -d /tmp/prep4submission_XXXXXX)"
dst="$tmp/$basedir/$matr"
mkdir -p "$dst"
cp -r "." "$dst"
arc_path=$(cd .. && pwd)
arc="$arc_path/${basedir}_${matr}.tar.gz"
if [ ! -w "$arc_path" ]; then
    echo >&2 "ERROR: cannot write to '$arc'"
    rm -rf "$tmp"
    exit 3
fi
tar -czf "$arc" -C "$tmp" "$basedir"
echo >&2 "archive successfully created as '$arc'"

# clean up
rm -rf "$tmp"

exit 0
