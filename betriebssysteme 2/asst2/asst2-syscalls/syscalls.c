#include <time.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdint.h>
#include <stddef.h> /* for NULL */
#include <stdio.h>
#include <unistd.h>

#include "syscalls.h"


void print3( char a, uint8_t b, int8_t c )
{
	printf("%c %u %d\n", a, b, c);
}

int count_digits(int n) {
	unsigned count = 0;
	while (n /= 10) {
		++count;
	}
	return count+1;
}

int uint_to_ascii(int n, char* ascii, int length, int fillto) {
	int digits = count_digits(n);
	int zeros = fillto > 0 ? fillto - digits : 0;

	// fill with zeros
	int i;
        for (i = 0; i < zeros; ++i) {
                ascii[i] = '0';
        }

	if (n == 0) {
		// if the number is zero
		ascii[i++] = '0';
		return i;
	}
	else {
		// while there are other digits and there is space left
		for (i = 0; n > 0 && i < length; ++i) {
			// get the last digit
			int digit = n % 10;
			n = (n-digit)/10;

			// add it to the buffer in ASCII format
			ascii[zeros+digits-i-1] = digit + '0';
		}
		return digits+zeros;
	}
}

int int_to_ascii(int n, char* ascii, int length, int fillto) {
	if (length == 0) {
		// if there is no space left in the string
		return 0;
	}
	else if (n < 0) {
		// negative
		ascii[0] = '-';
		return uint_to_ascii(-n, ascii+1, length, fillto-1) + 1;
	}
	else {
		// positive or zero
		return uint_to_ascii(n, ascii, length, fillto);
	}
}

void write3( char a, uint8_t b, int8_t c )
{
	// 2 for the char and the space, then 4 for the unsigned integer and a space,
	// then 5 for the signed integer and a space, and then 2 for a newline and a null byte.
	int length = 2+4+5+2+1;

	char ascii[length];
	int i = 0;
	ascii[i++] = a;
	ascii[i++] = ' ';
	i += int_to_ascii(b, ascii+i, length-i, 0);
	ascii[i++] = ' ';
	i += int_to_ascii(c, ascii+i, length-i, 0);
	ascii[i++] = '\n';
	ascii[i++] = 0;

	write(1, ascii, i);
}


char* getTime()
{
	// two for day, month, hour, minute, second, 4 for the year, and whitespaces and separators
	int length = 2+2+2+2+2+4+7;
	char* datestring = (char*) malloc(length);

	struct timeval thetime;
	gettimeofday(&thetime, 0);
	struct tm* tm;
	tm = localtime(&thetime.tv_sec);

	int i = 0;
	i += int_to_ascii(tm->tm_mday, datestring+i, length-i, 2);
	datestring[i++] = '.';
	i += int_to_ascii(tm->tm_mon+1, datestring+i, length-i, 2);
	datestring[i++] = '.';
	i += int_to_ascii(tm->tm_year+1900, datestring+i, length-i, 4);
	datestring[i++] = ' ';
	datestring[i++] = '-';
	datestring[i++] = ' ';
	i += int_to_ascii(tm->tm_hour, datestring+i, length-i, 2);
	datestring[i++] = ':';
	i += int_to_ascii(tm->tm_min, datestring+i, length-i, 2);
	datestring[i++] = ':';
	i += int_to_ascii(tm->tm_sec, datestring+i, length-i, 2);
	datestring[i++] = 0;

	return datestring;
}
