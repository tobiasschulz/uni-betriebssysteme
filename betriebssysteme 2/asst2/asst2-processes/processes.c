#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/types.h>

#include "processes.h"


/* read 'man setrlimit' before running this function! */
void forkbomb() {
	while (fork() >= 0) { fork(); }
}

void spawnChildren( unsigned int n ) {
	for (; n > 0; --n) {
		pid_t process_id_of_child = fork();
		if (process_id_of_child) {
			// das wird im Parent-Prozess ausgefuehrt
			printf("I am the parent process of: %d\n", process_id_of_child);
			// wegen den Zombies
			waitpid(-1, NULL, 0);
		}
		else {
			// dies ist der Child-Prozess
			process_id_of_child = getpid();
			printf("%d\n", process_id_of_child);
			exit(0);
		}
	}
}
