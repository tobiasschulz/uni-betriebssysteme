#include "bits.h"

#include <stdio.h>

static void printBytes(uint8_t b[3]) {
	printf("%02x %02x %02x\n", (int) b[0], (int) b[1], (int) b[2]);
}

int main()
{
	uint8_t b[] = {0x00, 0x01, 0x02};
	printf("Ursprungsarray:\n");
	printBytes(b);
	
	printf("setN(b, 2);\n");
	setN(b, 2);
	printBytes(b);

	printf("setN(b, 7);\n");
	setN(b, 7);
	printBytes(b);

	printf("setN(b, 12);\n");
	setN(b, 12);
	printBytes(b);

	printf("clrN(b, 7);\n");
	clrN(b, 7);
	printBytes(b);
	
	return 0;
}
