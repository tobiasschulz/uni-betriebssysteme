/*
 * This is a simple backtracking sudoku solver.
 * It demonstrates how a simple C-Program may be layed out
 * as well as how to work with bit-fields.
 * 
 * Compile with: gcc -W -Wall -Werror -std=c99 sudoku.c -o sudoku
 * Run by passing a sudoku game into stdin:
 * 	- Create a file 'in.txt' which contains 9x9 characters
 * 		- A number 0-9 for each fixed position in the sudoku
 * 		- The 'x' character for a variable position
 * 	- Run the solver with: ./sudoku < in.txt
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char board[10][10]; /* a little more - for \0 for instance ;-) */
unsigned int rows[9], columns[9], fields[9];

void printboard ()
{
	for( int i=0; i<9; ++i )
		printf( "%s\n", board[i] );
}


/* remove value from x,y coordinate */
void clear ( int x, int y, int val )
{
	unsigned int mask = ~(1 << val);
	int field = (y/3)*3 + x/3;

	rows[x]       &= mask;
	columns[y]    &= mask;
	fields[field] &= mask;
	board[x][y]    = 'x';
}


/* add value to x,y coordinate */
void set ( int x, int y, int val )
{
	unsigned int mask = 1 << val;
	int field = (y/3)*3 + x/3;

	rows[x]       |= mask;
	columns[y]    |= mask;
	fields[field] |= mask;
	board[x][y]    = val + '0';
}


/* check if value is already in row, column or field */
int new_ok ( int x, int y, int val )
{
	unsigned int mask = 1 << val;
	int field = (y/3)*3 + x/3;

	// val already in row?
	if( (rows[x] & mask) )
		return 0;
		
	// val already in column?
	if( (columns[y] & mask) )
		return 0;

	// val already in field?
	if( (fields[field] & mask) )
		return 0;

	// val may be valid, insert and continue with backtrack
	set( x, y, val );
	return 1;
}


int solve ( int pos )
{
	// this is the last position - we're done :)
	if( pos == 81 )
		return 1;

	int x = pos % 9;
	int y = pos / 9;

	// if the value is given for this position:
	// 	skip and go to next field
	if( board[x][y] != 'x' )
		return solve( pos+1 );

	// otherwise:
	// 	try all values
	for( int i=1; i<=9; ++i )
	{
		if( new_ok(x, y, i) )
		{
			if( solve(pos+1) )
				return 1;
			
			clear( x, y, i );
		}
	}

	return 0;
}


int main ()
{
	memset( rows,    0, sizeof rows    );
	memset( columns, 0, sizeof columns );
	memset( fields,  0, sizeof fields  );
	memset( board,   0, sizeof board   );

	for( int i=0; i<9; ++i )
	{
		if ( scanf( "%s", board[i] ) ==-1) { exit(EXIT_FAILURE);}

		for( int j=0; j<9; ++j )
			if( board[i][j] != 'x' )
				set( i, j, board[i][j] - '0' );
	}

	if( solve(0) )
	{
		printf( "a solution is:\n" );
		printboard();
	}
	else
	{
		printf( "no solution found\n" );
	}

	return 0;
}
