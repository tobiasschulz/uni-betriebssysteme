#include "bits.h"
#include <stdio.h>
#include <inttypes.h>

static void printBytes(uint8_t b[3], unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; ++i) {
		printf("%02x ", (int) b[i]);
	}
	printf("%s", "\n");
}

int main()
{
	/*
	 * Put your test code here.
	 */
	printf("roundDown(9000) = %d\n", roundDown(9000));

	uint8_t b[] = {0x00, 0x01, 0x02};
	unsigned int size = 3;

	printf("Ursprungsarray:\n");
	printBytes(b, size);

	printf("getN(b, 15) = %d\n", getN(b, 15));
	printBytes(b, size);

	printf("setN(b, 2);\n");
	setN(b, 2);
	printBytes(b, size);

	printf("setN(b, 7);\n");
	setN(b, 7);
	printBytes(b, size);

	printf("setN(b, 12);\n");
	setN(b, 12);
	printBytes(b, size);

	printf("clrN(b, 7);\n");
	clrN(b, 7);
	printBytes(b, size);

	uint64_t i = 1;
	rot(&i, 1);
	printf("rot(%d, 1) = %"PRIu64"\n", 1, i);
	printBytes(b, size);

	i = 1;
	rot(&i, -1);
	printf("rot(%d, -1) = %"PRIu64"\n", 1, i);
	printBytes(b, size);


	return 0;
}
