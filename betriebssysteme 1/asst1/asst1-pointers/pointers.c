#include "pointers.h"

int average ( int *arrayPointer, unsigned int size )
{
	if (size == 0)
		return 0;

	int sum = 0;
	unsigned int i;
	for (i = 0; i < size; ++i)
	{
		sum += arrayPointer[i];
	}

	return (sum - sum%size) / size;
}


int p_average ( int **pointerArray, unsigned int size )
{
	if (size == 0)
		return 0;

	int sum = 0;
	unsigned int i;
	for (i = 0; i < size; ++i)
	{
		sum += *pointerArray[i];
	}

	return (sum - sum%size) / size;
}
