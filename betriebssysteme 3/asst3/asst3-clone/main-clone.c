#define _GNU_SOURCE
#include <stdio.h>
#include <inttypes.h> /* for PRIu64 and uint64_t */
#include <sched.h>
#include <stdlib.h>
#include <sys/wait.h>
/* you'll need further includes */

#include "tally.h"


#define THREADS 2
#define STACK_SIZE 65535

int adapterFunction(void *args){
	uint64_t *arg =(uint64_t*) args;
	increment((uint64_t*)arg[1],arg[0]);
	return 0;
}

int main ()
{
	uint64_t N = 10000000;
	uint64_t tally = 0;
	void* stackAdress[THREADS];
	int pid[THREADS];
	uint64_t  argContainer[2];
	argContainer[0]=N;
	argContainer[1]=(uint64_t)&tally;
	tally_init();

	for(int i=0;i<THREADS;i++){
		stackAdress[i]=malloc(STACK_SIZE);
		pid[i]=clone(&adapterFunction,(stackAdress[i]+STACK_SIZE),SIGCHLD | CLONE_FILES | 
				CLONE_VM,argContainer);
	}
	for(int i=0;i<THREADS;i++){
		waitpid(pid[i],NULL,0);
		free(stackAdress[i]);
	}

	tally_clean();

	printf( "Tally is %" PRIu64 "\n", tally );
	return 0;
}

