#define _GNU_SOURCE
#include <inttypes.h>

#include "tally.h"

#include <semaphore.h>
#include <stdio.h>

sem_t sem; // semaphore

void tally_init ()
{
	sem_init(&sem, 0, 1);
}


/* modify this function to achieve correct parallel incrementation */
void increment ( uint64_t *tally, uint64_t steps )
{
	for( uint64_t i = 0; i < steps; i++ )
	{
		sem_wait(&sem);
		*tally += 1;
		sem_post(&sem);
	}
}


void tally_clean ()
{
	sem_destroy(&sem);
}
